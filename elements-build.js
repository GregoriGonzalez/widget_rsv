const fs = require('fs-extra');  
const concat = require('concat');

(async function build() {
    const files = [
        './dist/widgetIE/runtime.js',
        './dist/widgetIE/polyfills-es5.js',
        './dist/widgetIE/polyfills.js',
        './dist/widgetIE/main.js'
    ];

    await fs.ensureDir('elements');
    await concat(files, 'elements/widget-reservandonos.js');
    await fs.copyFile(
        './dist/widgetIE/styles.css',
        'elements/widget-reservandonos.css'
    );
})();