import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-errorredeemcode-dialog',
  templateUrl: './errorredeemcode-dialog.component.html',
  styleUrls: ['./errorredeemcode-dialog.component.scss'],
})
export class ErrorRedeemCodeDialogComponent implements OnInit {
  tags;
  constructor(
    public dialogRef: MatDialogRef<ErrorRedeemCodeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {}

  closeDialog(): void {
    this.dialogRef.close(false);
  }
  confirmed(): void {
    this.dialogRef.close(true);
  }
}
