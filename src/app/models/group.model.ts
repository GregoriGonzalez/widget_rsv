import { Table } from './table.model';

export class Group {
  public id: number;
  public numbers: string;
  public minCapacity: number;
  public maxCapacity: number;
  public layoutId: number;
  public placeId: number;
  public tables: Table[];
  public tableIds: number[];

  constructor() {}
}
