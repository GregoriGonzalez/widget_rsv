import { Place } from './place.model';
import { Moment } from 'moment';

export class GiftCard {
  public id?: number;
  public beneficiaryName: string;
  public email: string;
  public countryCode: string;
  public phone: string;
  public amount: number;
  public token?: string;
  public redeemed?: boolean;
  public paymentTokenId?: string;
  public placeId?: number;
  public place?: Place;
  public createdAt?: Moment;
  constructor() {}
}

export class GiftCardResult {
  public count: number;
  public rows: GiftCard[];
}
