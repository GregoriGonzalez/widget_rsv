import { Service } from './service.model';
import { Zone } from './zone.model';
import { Layout } from './layout.model';

export class ServiceZone {
  public serviceId: number;
  public zoneId: number;
  public layoutId: number;
  public smokeArea: boolean;
  public service: Service;
  public zone: Zone;
  public layout: Layout;

  constructor() {}
}
