import { Moment } from 'moment';
import { Customer } from './customer.model';
import { Table } from './table.model';
import { Place } from './place.model';
import { Partner } from './partner.model';
import { Service } from './service.model';
import { Zone } from './zone.model';
import { Group } from './group.model';

export class Reservation {
  public id: number;
  public datetime: Moment | string;
  public date: string;
  public start: number;
  public end: number;
  public peopleCount: number;
  public duration: number;
  public isGroup: boolean;
  public status: string; // NO_SHOW, CANCELLED, CONFIRMED, RE_CONFIRMED, ARRIVED, SEATED, GONE
  public type: string; // FREE, CANCELLATION_POLICY
  public origin: string; // WEBSITE, PHONE, WALK-IN
  public newCostumer: boolean;
  public notes: string;
  public smokeArea?: boolean;

  public partnerId?: number;
  public partner?: Partner;
  public serviceId?: number;
  public service?: Service;
  public zoneId?: number;
  public zone?: Zone;
  public tableId?: number;
  public table?: Table;
  public groupId?: number;
  public group?: Group;
  public placeId?: number;
  public place?: Place;
  public customer?: Customer;
  public tables?: Table[];
  public createdAt?: Moment | string;

  public paymentTokenId?: string;
  public advancePayment?: boolean;
  public selectedCardId?: string;
  public adPayValue?: number;
  public utmSource?: string;
  public utmMedium?: string;
  public utmCampaign?: string;
  public utmTerm?: string;

  public notificationEmail?: boolean;

  constructor() {}
}

export class ReservationResult {
  public rows: Reservation[];
  public count: number;
}
