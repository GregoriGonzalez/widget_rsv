export class Sponsor {
  public id: number;
  public name: string;
  public isActive: boolean;
  public placeId: number;
  public sponsorId: number;
  public urlBase: string;
  constructor() {}
}
