import { Zone } from './zone.model';
import { Moment } from 'moment';

export class Service {
  public id: number;
  public name: string;
  public repetition: string;
  public weekdays: string;
  public weekdaysArray: string[];
  public date: Moment;
  public start: number;
  public end: number;
  public tablesCount: number;
  public maxCapacity: number;
  public placeId: number;
  public zones: Zone[];

  constructor() {}
}
