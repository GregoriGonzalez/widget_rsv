import { Layout } from './layout.model';
import { ServiceZone } from './service-zone.model';
import { TableGroup } from './table.model';

export class Zone {
  public id: number;
  public name: string;
  public smokeArea: boolean;
  public placeId: number;
  public layouts: Layout[];
  public tableGroups: TableGroup[];
  public ServiceZone: ServiceZone;

  constructor() {}
}
