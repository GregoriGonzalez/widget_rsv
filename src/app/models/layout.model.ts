import { Zone } from './zone.model';
import { Table } from './table.model';
import { Group } from './group.model';

export class Layout {
  public id: number;
  public name: string;
  public tablesCount: number;
  public maxCapacity: number;
  public placeId: number;
  public zoneId: number;
  public zone: Zone;
  public tables: Table[];
  public groups: Group[];

  constructor() {}
}
