import { Reservation } from './reservation.model';

export class Customer {
  public id: number;
  public fullname: string;
  public countryCode: string;
  public phone: string;
  public email: string;
  public notes: string;
  public reservations?: Reservation[];
  constructor() {}
}

export class CostumerResult {
  public count: number;
  public rows: Customer[];
}
