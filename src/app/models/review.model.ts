import { Reservation } from './reservation.model';
import { Place } from './place.model';

export class Review {
  public id: number;
  public score: number;
  public comment: string;
  public createdAt: Date;
  public reservationId: number;
  public placeId: number;
  public reservation: Reservation;
  public place: Place;
  constructor() {}
}

export class ReviewResult {
  public count: number;
  public rows: Review[];
}
