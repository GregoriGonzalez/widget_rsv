export class ConektaCard {
  public number: string;
  public name: string;
  public 'exp_year': string;
  public 'exp_month': string;
  public cvc: string;
  public address: {
    street1: string;
    street2: string;
    city: string;
    state: string;
    zip: string;
    country: string;
  };
  constructor() {}
}

export class ConektaClient {
  public name: string;
  public email: string;
  public phone: string;
  public metadata?: {
    description: string,
    reference: string
  };
  public 'payment_sources': {
    type: string,
    token_id: string
  }[];

  constructor() {}
}

export class ConektaToken {
  public id: string;
  public object: string;
  public used: boolean;
  public livemode: boolean;
  constructor() {}
}

export class ConektaError {
  public type: string;
  public message: string;
  public 'message_to_purchaser': string;
  public 'error_code': string;
  public param: string;
  constructor() {}
}
