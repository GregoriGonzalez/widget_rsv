import { Plan } from './plan.model';
import { User } from './user.model';
import { Sponsor } from './sponsor.model';

export class Place {
  public id: number;
  public ownerId: number;
  public planId: number;
  public name: string;
  public address: string;
  public phone: string;
  public email: string;
  public logoImage: string;
  public dueDate: Date;
  public monthlyPlanPrice: number;
  public sentPersonPrice: number;
  public plan: Plan;
  public owner: User;
  public hasBooking: boolean;

  public timeBeforeClose: number;
  public timezone: string;
  public openingDays: OpeningDay[];
  public stayTimes: StayTime[];
  public placeLock: [];
  public sponsorsPlaces: Sponsor[];

  public cancellationPolicyEnable: boolean;
  public cancellationFeePerPerson: number;
  public numberPersonPolicyCancellation: number;
  public advancePaymentPolicyEnable: boolean;
  public advancePaymentPolicyFeePerPerson: number;
  public advancePaymentPolicyNumberPeople: number;
  public priorityPaymentWidget: string;

  public paymentGatewayPublicKey: string;
  public paymentGatewayPrivateKey: string;
  public typeWidget: number;

  public theme: string;

  public primaryColor?: string;
  public secondaryColor?: string;

  public intervalWidget?: number;
  
  public maxPeopleReservations?: number;

  constructor() {}
}

export class OpeningDay {
  public weekday: string;
  public start: number;
  public end: number;
  public start2: number;
  public end2: number;
  public smokeArea: boolean;

  constructor() {}
}

export class StayTime {
  public peopleCount: number;
  public time: number;

  constructor() {}
}
