export class User {
  public id: number;
  public email: string;
  public password: string;
  public fullname: string;
  public address: string;
  public phone: string;
  public profileImage: string;
  public placeId: number;
  public placeLogo?: string;
  public role: string;
  public unreadedNotificationsCount: number;
  constructor() {}
}
