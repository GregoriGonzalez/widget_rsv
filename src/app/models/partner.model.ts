import { Reservation } from './reservation.model';

export class Partner {
  public id: number;
  public name: string;
  public contact: string;
  public countryCode: string;
  public phone: string;
  public email: string;
  public notes: string;
  public clabe: string;
  public beneficiary: string;
  public sentPersonFee: string;
  public reservations: Reservation[];

  constructor() {}
}

export class PartnerResult {
  public count: number;
  public rows: Partner[];
}
