export class Plan {
  public id: number;
  public name: string;
  public monthlyPlanPrice: number;
  public sentPersonPrice: number;
  constructor() {}
}
