export class Table {
  public id: number;
  public number: number;
  public minCapacity: number;
  public maxCapacity: number;
  public tableTypeId: number;
  public tableHeightId: number;
  public layoutId: number;
  public placeId: number;
  public isVertical: boolean;
  public tableHeight: TableHeight;
  public tableType: TableType;
  public coordX: number;
  public coordY: number;

  constructor() {}
}

export class TableHeight {
  public id: number;
  public name: string;

  constructor() {}
}

export class TableType {
  public id: number;
  public name: string;

  constructor() {}
}

export class TableGroup {
  public isGroup: boolean;
  public id: number;
  public numbers: string;
  public minCapacity: number;
  public maxCapacity: number;
  public zoneId: number;
  public zoneName: string;
  public smokeArea: boolean;
}
