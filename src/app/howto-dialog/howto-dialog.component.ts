import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-howto-dialog',
  templateUrl: './howto-dialog.component.html',
  styleUrls: ['./howto-dialog.component.scss'],
})
export class HowtoDialogComponent implements OnInit {
  tags;
  constructor(
    public dialogRef: MatDialogRef<HowtoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {}

  closeDialog(): void {
    this.dialogRef.close(false);
  }
  confirmed(): void {
    this.dialogRef.close(true);
  }

  urlBuildImage(param): string{
    return `${environment.urlImage}/${param}`;
  }
}
