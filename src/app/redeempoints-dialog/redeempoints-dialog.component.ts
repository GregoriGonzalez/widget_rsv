import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-redeempoints-dialog',
  templateUrl: './redeempoints-dialog.component.html',
  styleUrls: ['./redeempoints-dialog.component.scss'],
})
export class RedeemPointsDialogComponent implements OnInit {
  tags;
  constructor(
    public dialogRef: MatDialogRef<RedeemPointsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {}

  closeDialog(): void {
    this.dialogRef.close(false);
  }
  confirmed(): void {
    this.dialogRef.close(true);
  }
  urlBuildImage(param): string{
    return `${environment.urlImage}/${param}`;
  }
}
