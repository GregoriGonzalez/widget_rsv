import { BrowserModule } from '@angular/platform-browser';
import {
  NgModule,
  // Injector, CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';

// import { CommonModule, registerLocaleData } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from '@angular/cdk/layout';

// import { createCustomElement } from '@angular/elements';

import { AppComponent } from './app.component';
// import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Angular
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Material
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { MatMomentDateModule } from '@angular/material-moment-adapter';

//
import { RegularForPipe } from './pipes/regular-for.pipe';
import { MomentFormatPipe } from './pipes/moment-format.pipe';
import { MinutesToTimePipe } from './pipes/minutes-to-time.pipe';
import { MatDialogModule } from '@angular/material/dialog';
import { HowtoDialogComponent } from './howto-dialog/howto-dialog.component';
import { RedeemPointsDialogComponent } from './redeempoints-dialog/redeempoints-dialog.component';
import { ErrorRedeemCodeDialogComponent } from './errorredeemcode-dialog/errorredeemcode-dialog.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

@NgModule({
  declarations: [
    AppComponent,
    //
    RegularForPipe,
    MomentFormatPipe,
    MinutesToTimePipe,
    HowtoDialogComponent,
    RedeemPointsDialogComponent,
    ErrorRedeemCodeDialogComponent
  ],
    imports: [
        BrowserModule,
        // NoopAnimationsModule,
        BrowserAnimationsModule,

        // Angular
        // CommonModule,
        HttpClientModule,
        ReactiveFormsModule,
        LayoutModule,
        FlexLayoutModule,
        // Material
        MatDatepickerModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatSelectModule,
        MatCardModule,
        MatDividerModule,
        MatTabsModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        //
        MatMomentDateModule,
        MatDialogModule,
        FormsModule,
        NgxMatSelectSearchModule
    ],
  // schemas: [
  //   CUSTOM_ELEMENTS_SCHEMA,
  // ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'es-MX' }],
  entryComponents: [HowtoDialogComponent, RedeemPointsDialogComponent, ErrorRedeemCodeDialogComponent],
  // entryComponents: [AppComponent],
  bootstrap: [AppComponent],
})
export class AppModule {
  // constructor(private injector: Injector) {
  //   const el = createCustomElement(AppComponent, { injector });
  //   customElements.define('new-widget', el);
  // }
  // ngDoBootstrap() {}
}
