import { FormGroup, FormControl, AbstractControl, ValidationErrors } from '@angular/forms';
// declare const Conekta: any;

// @dynamic
export class Validations {
  public static validateForm(form: FormGroup): void {
    Object.keys(form.controls).forEach(field => {
      const control = form.get(field);
      if ((control as FormGroup).controls) {
        this.validateForm(control as FormGroup);
      }
      control.markAsTouched({ onlySelf: true });
    });
  }

  /* public static cardNumber(control: FormControl): {invalidCardNumber: boolean} {
    Conekta.setLanguage('es');
    return control.value === '' || Conekta.card.validateNumber(control.value) ? null : {invalidCardNumber: true};
  }

  public static cardExpirationDate(expMonthName: string, expYearName: string): (control: AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      if (!control.parent || !control.parent.value) {
        return {invalidExpirationDate: true};
      }

      const expMonth = control.parent.controls[expMonthName].value;
      const expYear = control.parent.controls[expYearName].value;
      Conekta.setLanguage('es');
      return Conekta.card.validateExpirationDate(expMonth, expYear) ? null : {invalidExpirationDate: true};
    };
  }

  public static cardCvc(control: FormControl): {invalidCvc: boolean} {
    Conekta.setLanguage('es');
    return control.value === '' || Conekta.card.validateCVC(control.value) ? null : {invalidCvc: true};
  }*/
}
