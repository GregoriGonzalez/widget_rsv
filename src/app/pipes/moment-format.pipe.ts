import { Pipe, PipeTransform } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { Moment } from 'moment';

import esMX from '@angular/common/locales/es-MX';
import esMXExtra from '@angular/common/locales/extra/es-MX';
import * as moment_ from 'moment';
const moment = moment_;

// registerLocaleData(esMX, esMXExtra);
// moment.locale('es-MX');

@Pipe({
  name: 'momentFormat'
})
export class MomentFormatPipe implements PipeTransform {
  transform(value: Moment | string, format: string): string {
    return moment(value).format(format);
  }
}
