import { Pipe, PipeTransform } from '@angular/core';
import { Moment } from 'moment';
import * as moment_ from 'moment';
const moment = moment_;

@Pipe({
  name: 'minutesToTime'
})
export class MinutesToTimePipe implements PipeTransform {
  transform(minutes: number): Moment {
    return moment().startOf('day').add(minutes, 'minutes');
  }
}
