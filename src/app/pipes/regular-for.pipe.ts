import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'regularFor'
})
export class RegularForPipe implements PipeTransform {

  transform(value: number, start = 0, step = 1): number[] {
    const array: number[] = [];
    for (let i = start; i <= value; i += step) {
      array.push(i);
    }
    return array;
  }

}
