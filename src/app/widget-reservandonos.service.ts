import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Service } from './models/service.model';
import { ServiceZone } from './models/service-zone.model';
import { Place } from './models/place.model';
import { Zone } from './models/zone.model';
import { Reservation } from './models/reservation.model';
import { GiftCard } from './models/gift-card.model';
import {
  ConektaCard,
  ConektaToken,
  ConektaError,
} from './models/conekta.model';
import { environment } from 'src/environments/environment';
declare const Conekta: any;

@Injectable({
  providedIn: 'root',
})
export class WidgetReservandonosService {
  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
    console.log('v1.6', this.apiUrl);
  }

  getServicesByDate(date: string, placeId = 0) {
    return this.http.get<Service[]>(`${this.apiUrl}/services/by-date/${date}`, {
      params: {
        placeId: placeId.toString(),
      },
    });
  }

  getServiceZones(serviceId: number, placeId = 0) {
    return this.http.get<ServiceZone[]>(
      `${this.apiUrl}/services/${serviceId}/zones`,
      {
        params: {
          placeId: placeId.toString(),
        },
      }
    );
  }

  getPlaceById(id: number) {
    return this.http.get<Place>(`${this.apiUrl}/settings/${id}`);
  }

  getWidgetConfig(id: number) {
    return this.http.get<Place>(`${this.apiUrl}/settings/${id}/widget-v1`);
  }

  getPlacesGroupBusiness(id: number) {
    return this.http.get<any>(`${this.apiUrl}/settings/groupBusiness/${id}`);
  }

  getSmartTables(serviceId: number, params) {
    return this.http.get<Zone[]>(
      `${this.apiUrl}/services/${serviceId}/smart-tables`,
      {
        params,
      }
    );
  }

  getSuggestions(placeId: number, params) {
    return this.http.get<any>(
      `${this.apiUrl}/services/${placeId}/suggestions`,
      {
        params,
      }
    );
  }

  saveReservation(reservation: Reservation) {
    if (!reservation.id) {
      return this.http.post<any>(`${this.apiUrl}/reservations/`, reservation);
    } else {
      return this.http.put<any>(`${this.apiUrl}/reservations/`, reservation);
    }
  }

  getPublicKey(placeId: number) {
    return this.http.get<{ paymentGatewayPublicKey: string }>(
      `${this.apiUrl}/settings/${placeId}/public-key`
    );
  }

  tokenize(card: ConektaCard, placeId: number): Promise<ConektaToken> {
    return new Promise(async (resolve, reject) => {
      this.getPublicKey(placeId).subscribe((data) => {
        Conekta.setPublicKey(data.paymentGatewayPublicKey);
        Conekta.Token.create(
          { card },
          (token: ConektaToken) => {
            resolve(token);
          },
          (error: ConektaError) => {
            reject(error);
          }
        );
      });
    });
  }

  getAllFromPlace(placeId) {
    return this.http.get<any>(`${this.apiUrl}/reviews/${placeId}/all`);
  }

  createGiftCard(giftCard: GiftCard) {
    return this.http.post<GiftCard>(`${this.apiUrl}/gift-cards`, giftCard);
  }

  checkStatusCode(id, code) {
    return this.http.post<any>(
      `${this.apiUrl}/registeredCustomer/id/${id}/code`,
      { code }
    );
  }

  changeCode(requestBody) {
    return this.http.post<any[]>(
      `${this.apiUrl}/registeredCustomer/ChangeCode`,
      requestBody
    );
  }
}
