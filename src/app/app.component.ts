import {AfterViewInit, Component, ElementRef, Inject, OnInit} from '@angular/core';
import * as moment from 'moment';
import {Moment} from 'moment';
import {Service} from './models/service.model';
import {OpeningDay, Place} from './models/place.model';
import {ServiceZone} from './models/service-zone.model';
import {TableGroup} from './models/table.model';
import {Reservation} from './models/reservation.model';
import {Customer} from './models/customer.model';
import {Countries, CountryCode} from './models/country-code';
import {Validations} from './helpers/validations';

import {WidgetReservandonosService} from './widget-reservandonos.service';
// import { ConektaCard, ConektaToken } from './models/conekta.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import tinycolor from 'tinycolor2';
import {MatDialog} from '@angular/material/dialog';
import {HowtoDialogComponent} from './howto-dialog/howto-dialog.component';
import {RedeemPointsDialogComponent} from './redeempoints-dialog/redeempoints-dialog.component';
import {ErrorRedeemCodeDialogComponent} from './errorredeemcode-dialog/errorredeemcode-dialog.component';
import {DOCUMENT} from '@angular/common';
import {environment} from '../environments/environment';
import {loadStripe} from '@stripe/stripe-js';

moment.locale('es');

export interface Color {
  name: string;
  hex: string;
  darkContrast: boolean;
}

@Component({
  selector: 'rsv-widget-reservas',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  arrayPlaces: any = [];
  charge: any = 0;
  countryCodes: CountryCode[] = Countries;
  dataUser: any = {
    fullname: null,
    code: '+52',
    phone: null,
    email: null,
  };
  displayProgressSpinner = false;
  duration = 60;
  filteredServiceZones: ServiceZone[] = [];
  form: FormGroup;
  hasBooking: any;
  maxDate: Moment;
  minDate: Moment;
  msg: any = '';
  origin = 'WEBSITE'; // input
  origins = [
    'PHONE',
    'GOOGLE',
    'WALK-IN',
    'WEBSITE',
    'WHATSAPP',
    'APP',
    'FACEBOOK',
    'TWITTER',
    'INSTAGRAM',
  ];
  paymentConditional: any = '';
  place: Place;
  placeId: number; // input
  planFree = false;
  referralId: number; // input
  reservation: Reservation;
  selectedTableGroup: TableGroup;
  services: Service[] = [];
  selectedPeopleCount: any = 2;
  selectedService: Service;
  serviceZones: ServiceZone[] = [];
  showTitle = false;
  smokeArea = false;
  step = 'loading';
  times = [];
  theme: string;
  viewPayment: any = '';
  viewList: any = false;
  widgetBasic: any = true;
  selectedSucursal: any;
  domainReservandonos = false;
  err: any;
  suggestion: any = [];
  imgUrl = '';
  placeBlockFree: boolean;
  days: any = [
    'sunday',
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
    'friday',
    'saturday',
  ];
  daysLock: any = [];
  coca: any;
  utms: any = {
    utmSource: '',
    utmMedium: '',
    utmCampaign: '',
    utmTerm: '',
  };
  maxPeopleReservations: any = 20;

  responseCode: any;
  disabeldI = false;
  disabeldemail = false;
  sessionCustomer = false;
  placeURL;
  referralURL;
  originURL;
  bonusCodePoints = 0;
  isActiveCode = 0;
  showProfile;
  groupBusinessURL: any;
  groupBusinessId: any;
  loading = false;

  formNewCreditCardStripe: FormGroup;
  stripeCreditCard = {
    clientSecret: null,
    cardNumber: null,
    cardExp: null,
    cardCvc: null,
  };

  PATTERN_ALMOST_TWO_NAMES = /^([a-zA-ZáéíóúÁÉÍÓÚ]+ [a-zA-ZáéíóúÁÉÍÓÚ]+([ a-zA-ZáéíóúÁÉÍÓÚ]*)*?)$/;
  elementsStripe: any;
  paying: any;

  constructor(
    private elementRef: ElementRef,
    private widgetService: WidgetReservandonosService,
    public dialog: MatDialog,
    @Inject(DOCUMENT) document: any,
    private fb: FormBuilder,
  ) {
    const url = new URL(document.location.href);
    this.placeURL = url.searchParams.get('place');
    this.referralURL = url.searchParams.get('referral');
    this.originURL = url.searchParams.get('origin');
    this.showProfile = url.searchParams.get('showProfile');
    this.groupBusinessURL = url.searchParams.get('groupBusiness');
    this.sessionCustomer = false;
  }

  payWithStripeNewCard(): void {}

  createStripeElement(): void {
    const style = {
      base: {
        color: '#000000',
        fontWeight: 400,
        fontFamily: 'Omnes, "Helvetica Neue", sans-serif',
        fontSize: '16px',
        '::placeholder': {
          color: '#E3E2EC',
        },
      },
      invalid: {
        color: '#dc3545',
      },
    };
    this.stripeCreditCard.cardNumber = this.elementsStripe.create('cardNumber', {
      placeholder: '4242 4242 4242 4242',
      style,
      classes: {
        base: 'input-stripe-custom'
      },
    });
    this.stripeCreditCard.cardExp = this.elementsStripe.create('cardExpiry', {
      placeholder: 'MM/AA',
      style,
      classes: {
        base: 'input-stripe-custom'
      },
    });
    this.stripeCreditCard.cardCvc = this.elementsStripe.create('cardCvc', {
      placeholder: '000',
      style,
      classes: {
        base: 'input-stripe-custom'
      },
    });

    this.stripeCreditCard.cardNumber.mount('#cardStripe');
    this.stripeCreditCard.cardExp.mount('#expStripe');
    this.stripeCreditCard.cardCvc.mount('#cvcStripe');

    this.stripeCreditCard.cardNumber.addEventListener('change', this.onChangeCard.bind(this));
    this.stripeCreditCard.cardExp.addEventListener('change', this.onChangeExp.bind(this));
    this.stripeCreditCard.cardCvc.addEventListener('change', this.onChangeCvv.bind(this));
  }

  public onChangeCard({error}: any): void {
    console.error(error);
    this.formNewCreditCardStripe.patchValue({cardNumber: !error});
    if (error) {
      this.formNewCreditCardStripe.controls?.cardNumber?.setErrors({
        card: error
      });
    }
    console.log(this.formNewCreditCardStripe.get('cardNumber'));
  }

  public onChangeCvv({error}: any): void {
    console.error(error);
    this.formNewCreditCardStripe.patchValue({cardCvc: !error});
  }

  public onChangeExp({error}: any): void {
    console.error(error);
    this.formNewCreditCardStripe.patchValue({cardExp: !error});
  }

  async loadStripe(): Promise<void>  {
    const stripe = await loadStripe('pk_test_51NqS5lC6MKCHgYwShAXq87h8xjTYKYeV0pVQcor7BThJkAxo0QbdPijHHg5VRjHu6n1fKe5LE2fMIGNmqZy0PReP00xLcWK9g4');
    this.elementsStripe = stripe.elements();

    this.formNewCreditCardStripe = this.fb.group({
      cardNumber: [false, [Validators.required, Validators.requiredTrue]],
      cardCvc: [false, [Validators.required, Validators.requiredTrue]],
      cardExp: [false, [Validators.required, Validators.requiredTrue]],
      cardName: ['', [Validators.required, Validators.pattern(this.PATTERN_ALMOST_TWO_NAMES)]],
      saveForFuturePayments: [true],
    });

    setTimeout(() => {
      this.createStripeElement();
    }, 3000);
  }

  public getImg(value: any): string {
    return `${environment.urlImage}/icons/${value}` || '';
  }

  traslate(value): string {
    let msg = '';
    switch (value) {
      case 'sunday':
        msg = 'domingo';
        break;
      case 'monday':
        msg = 'lunes';
        break;
      case 'tuesday':
        msg = 'martes';
        break;
      case 'wednesday':
        msg = 'miércoles';
        break;
      case 'thursday':
        msg = 'jueves';
        break;
      case 'friday':
        msg = 'viernes';
        break;
      case 'saturday':
        msg = 'sábado';
        break;
    }
    return msg;
  }

  downloadMenu(): void {
    window.open(this.coca, '_blank');
  }

  ngOnInit(): void {
    console.log('VERSION WIDGET: V2.1');
    const baseUrl = window.location.toString();
    this.domainReservandonos =
      baseUrl.includes('reservandonos') || baseUrl.includes('localhost');
    const originInput =
      this.originURL != null
        ? this.originURL
        : this.elementRef.nativeElement.getAttribute('origin');
    if (originInput && this.origins.indexOf(originInput) > -1) {
      this.origin = originInput;
    }

    this.groupBusinessId = this.groupBusinessURL != null ? this.groupBusinessURL : this.elementRef.nativeElement.getAttribute('groupBusiness');

    this.referralId =
      this.referralURL != null
        ? this.referralURL
        : this.elementRef.nativeElement.getAttribute('referral');
    this.placeId =
      this.placeURL != null
        ? this.placeURL
        : this.elementRef.nativeElement.getAttribute('place');

    this.placeURL = this.placeId ? this.placeId : null;
    console.log(this.referralId, '**', this.placeId, '**', this.groupBusinessId);

    this.minDate = moment();
    this.maxDate = moment().add(3, 'months').endOf('month');
    this.reservation = new Reservation();
    this.reservation.partnerId = this.referralId;

    this.getParams(window.location.href);
    this.initForm();
    this.loadStripe().then();
  }

  getFlag(): any {
    return `${environment.urlFlags}/${this.countryCodes.find((elem: any) => elem.code === this.form.get('customer')?.get('countryCode')?.value)?.iso}`;
  }

  getFlagByISO(iso: string): any {
    return `${environment.urlFlags}/${iso}`;
  }

  getCountries(): any {
    const value = this.form.value.customer?.ladasFilter;
    return value ? Countries.filter(elem => elem.name.toLocaleUpperCase().includes(value.toLocaleUpperCase())) : Countries;
  }


  ngAfterViewInit(): void {}

  getParams(url: any): void {
    const params: any = {};
    const parser = document.createElement('a');
    parser.href = url;

    const search = decodeURI(parser.search.substring(1));
    const vars = search.split('&');
    for (let i = 0; i < vars.length; i++) {
      const pair = vars[i].split('=');
      params[pair[0]] = decodeURIComponent(pair[1]);
    }

    if (!this.placeId && params?.place) {
      this.showTitle = true;
      this.placeId = params?.place;
      this.referralId = null;
    }

    if (params?.origin) {
      if (params?.origin === 'fb') {
        this.origin = 'FACEBOOK';
      } else if (params?.origin === 'APP') {
        this.origin = 'APP';
      }
    }

    if (params?.bookerWidget === 1) {
      this.dataUser = {
        fullname: params?.firstname
          ? `${params?.firstname} ${params?.lastname}`
          : null,
        code: params?.code ? params?.code : '+52',
        phone: params?.phone ? params?.phone : null,
        email: params?.email ? params?.email : '',
      };
    }

    if (
      params?.utm_source ||  params?.utm_medium ||  params?.utm_campaign ||  params?.utm_term ||
      this.getCookie('utm_source') || this.getCookie('utm_medium') || this.getCookie('utm_campaign') || this.getCookie('utm_term')
    ) {
      this.utms = {
        utmSource: params?.utm_source || this.getCookie('utm_source') || '',
        utmMedium: params?.utm_medium || this.getCookie('utm_medium') || '',
        utmCampaign: params?.utm_campaign || this.getCookie('utm_campaign') || '',
        utmTerm: params?.utm_term || this.getCookie('utm_term') || '',
      };
    }
  }

  compareHoursObjects(object1: any, object2: any): boolean {
    if (object1 && object2) {
      return object1?.id === object2?.id;
    }
    return false;
  }

  initForm(): void {
    if (this.showProfile === 'true') {
      let data;
      if (sessionStorage.getItem('reserv-auth') != null) {
        this.disabeldI = true;
        this.disabeldemail = true;
        this.sessionCustomer = true;

        data = JSON.parse(sessionStorage.getItem('reserv-auth'));
        this.dataUser = {
          fullname: data.user.name,
          code: data.user.lada,
          phone: data.user.phone,
          email: data.user.email,
          id: data.user.id, // registeredCustomerid
        };
      } else if (localStorage.getItem('reserv-auth') != null) {
        this.disabeldI = true;
        this.disabeldemail = true;
        this.sessionCustomer = true;

        data = JSON.parse(localStorage.getItem('reserv-auth'));
        this.dataUser = {
          fullname: data.user.name,
          code: data.user.lada,
          phone: data.user.phone,
          email: data.user.email,
          id: data.user.id, // registeredCustomerid
        };
      } else {
        this.disabeldI = false;
        this.disabeldemail = false;
      }
    }

    this.form = new FormGroup({
      reservation: new FormGroup({
        date: new FormControl(moment(), [Validators.required]),
        time: new FormControl(moment().add(1, 'hours').startOf('hour'), [
          Validators.required,
        ]),
        peopleCount: new FormControl(2, [Validators.required]),
        area: new FormControl('ANY', [Validators.required]),
        zoneId: new FormControl(0),
        tableTypeId: new FormControl(0),
        tableHeightId: new FormControl(0),
        sucursal: new FormControl(),
      }),
      customer: new FormGroup({
        fullname: new FormControl(
          { value: this.dataUser?.fullname, disabled: this.disabeldI },
          [Validators.required]
        ),
        countryCode: new FormControl(
          { value: this.dataUser?.code, disabled: this.disabeldI },
          [Validators.required]
        ),
        phone: new FormControl(
          { value: this.dataUser?.phone, disabled: this.disabeldI },
          [Validators.required, Validators.minLength(6)]
        ),
        email: new FormControl(
          {
            value: this.dataUser?.email,
            disabled: this.disabeldemail,
          },
          [Validators.required, Validators.email]
        ),
        notes: new FormControl(),
        codeToRedeemW: new FormControl(),
        ladasFilter: new FormControl()
      }),
      payment: new FormGroup({
        card: new FormGroup({
          number: new FormControl(),
          name: new FormControl(),
          exp_year: new FormControl(),
          exp_month: new FormControl(),
          cvc: new FormControl(),
          acceptCancellationPolicy: new FormControl(false),
        }),
      }),
    });

    if (this.groupBusinessId) {
      this.getGroupBusiness();
    }
    this.loadConfig();
    this.setValidators();
  }

  getGroupBusiness(): void {
    this.widgetService.getPlacesGroupBusiness(this.groupBusinessId).subscribe(places => {
      console.log('places', places);
      this.arrayPlaces = places;
      this.viewList = true;
      this.step = 'reservation';
    });
  }

  onCardDateChange(): void {
    this.form
      .get('payment')
      .get('card')
      .get('exp_year')
      .updateValueAndValidity();
    this.form
      .get('payment')
      .get('card')
      .get('exp_month')
      .updateValueAndValidity();
  }

  onDateChange(): void {
    // console.log('date change')
    this.placeBlockFree = false;
    if (this.planFree) {
      this.loadTimes();
    } else {
      this.loadServices();
    }
  }

  onTimeChange(): void {
    this.placeBlockFree = false;
    if (!this.planFree) {
      const time = this.form.value.reservation.time;
      const newService = this.services.find(
        (s) => s.start <= time && s.end > time
      );
      if (!this.selectedService || newService.id !== this.selectedService.id) {
        this.selectedService = newService;
        this.onServiceChange();
      } else {
        console.log('onTimeChange');
        this.loadSmartTables();
      }
    }
  }

  verifyPlaceLock(): any {
    const r = this.form.value.reservation;
    this.suggestion = [];
    return this.place.placeLock?.find(
      (elem: any) =>
        (
          (elem?.date === r.date.format('YYYY-MM-DD')) ||
          (elem?.isInterval && elem?.date <= r.date.format('YYYY-MM-DD') && elem?.dateEnd >= r.date.format('YYYY-MM-DD'))
        )
        &&
        elem?.start <= r.time &&
        r.time <= elem?.end
    );
  }

  verifyPlaceLockTime(time): any {
    const r = this.form.value.reservation;
    return this.place.placeLock?.find((elem: any) =>
      (
        (elem?.date === r.date.format('YYYY-MM-DD')) ||
        (elem?.isInterval && elem?.date <= r.date.format('YYYY-MM-DD') && elem?.dateEnd >= r.date.format('YYYY-MM-DD'))
      )
      &&
      elem?.start <= time && time <= elem?.end
    );
  }

  getCookie(cname): string {
    const name = `${cname}=`;
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  }


  onPeopleCountChange(): void {
    const f = this.form;
    const c = f.get('payment').get('card');
    // console.log('onPeopleCountChange');
    this.selectedPeopleCount = f.value.reservation.peopleCount;
    if (this.place && this.place.stayTimes && this.place.stayTimes.length) {
      const stayTime =
        this.place.stayTimes.find(
          (s) => s.peopleCount === this.selectedPeopleCount
        ) || this.place.stayTimes[this.place.stayTimes.length - 1];
      this.duration = stayTime.time || 60;
      // console.log('onPeopleCountChange loadSmartTables');
      if (!this.planFree) {
        console.log('onPeopleCountChange');
        this.loadSmartTables();
      } else {
        this.enabledSuggestions();
      }
    }

    this.setValidatorsPriority();

    this.reservation.type = 'FREE';
    if (
      !this.place.advancePaymentPolicyEnable &&
      !this.place.cancellationPolicyEnable
    ) {
      this.reservation.type = 'FREE';
    } else {
      if (
        this.place.cancellationPolicyEnable &&
        this.selectedPeopleCount >= this.place.numberPersonPolicyCancellation
      ) {
        this.reservation.type = 'CANCELLATION_POLICY';
        this.reservation.selectedCardId = 'new';
      } else if (
        this.place.advancePaymentPolicyEnable &&
        this.selectedPeopleCount >= this.place.advancePaymentPolicyNumberPeople
      ) {
        this.reservation.type = 'CANCELLATION_POLICY';
        this.reservation.selectedCardId = 'new';
      } else {
        this.reservation.type = 'FREE';
      }
    }
    // console.log(this.reservation.type);
  }

  onServiceChange(): void {
    console.log('onServiceChange');
    if (this.selectedService) {
      this.widgetService
        .getServiceZones(this.selectedService.id, this.placeId)
        .subscribe((serviceZones) => {
          this.serviceZones = serviceZones;
          this.filterServiceZones();
          this.loadSmartTables();
        });
    }
  }

  onAreaChange(): void {
    console.log('onAreaChange');
    this.filterServiceZones();
    this.loadSmartTables();
  }

  multiplication(number1, number2): number {
    return Math.round(number1 * number2);
  }

  setValidatorsPriority(): void {
    let meetsCancelation = false;
    let meetsAdvance = false;
    this.viewPayment = '';
    if (
      this.place.cancellationPolicyEnable &&
      this.place.numberPersonPolicyCancellation !== 0 &&
      this.selectedPeopleCount >= this.place.numberPersonPolicyCancellation
    ) {
      meetsCancelation = true;
      this.viewPayment = 'cancelation';
      this.reservation.selectedCardId = 'new';
      // console.log('cancelation new');
      if (this.place) {
        this.charge = this.multiplication(
          this.selectedPeopleCount,
          this.place.cancellationFeePerPerson
        );
      }
    }

    if (
      this.place.advancePaymentPolicyEnable &&
      this.place.advancePaymentPolicyNumberPeople !== 0 &&
      this.selectedPeopleCount >= this.place.advancePaymentPolicyNumberPeople
    ) {
      meetsAdvance = true;
      this.viewPayment = 'advance';
      // console.log('advance new');
      this.reservation.selectedCardId = 'new';
      if (this.place) {
        this.charge = this.multiplication(
          this.selectedPeopleCount,
          this.place.advancePaymentPolicyFeePerPerson
        );
      }
    }

    if (meetsCancelation && meetsAdvance) {
      if (this.paymentConditional === 'advance') {
        this.viewPayment = 'advance';
        if (this.place) {
          this.charge = this.multiplication(
            this.selectedPeopleCount,
            this.place.advancePaymentPolicyFeePerPerson
          );
        }
      } else {
        this.viewPayment = 'cancelation';
        if (this.place) {
          this.charge = this.multiplication(
            this.selectedPeopleCount,
            this.place.cancellationFeePerPerson
          );
        }
      }
    }

    if (!meetsAdvance && !meetsCancelation) {
      this.reservation.selectedCardId = null;
    }
  }

  changeSucursal(event): void {
    this.placeId = event.value?.id;
    this.reservation.type = 'FREE';
    this.loadConfig();
  }

  loadConfig(): void {
    if (this.placeId) {
      this.loading = true;
      this.widgetService.getWidgetConfig(this.placeId).subscribe(
        (config: any) => {
          this.loading = false;
          if (config) {
            const primary = config.primaryColor || '#eb3f58';
            const secondary = config.secondaryColor || '#ff4081';
            this.setThemeColors(primary, secondary);

            if (config?.length) {
              this.step = 'reservation';
              this.viewList = true;
              this.arrayPlaces = config;
            } else {
              this.loadPlaceConfig(config);
            }
          }
          else {
            this.placeURL = null;
          }
        },
        (err) => {
          console.error('err', err);
        }
      );
    }
  }

  setThemeColors(p, s): void {
    const primaryColorPalette = this.computeColors(p);
    const secondaryColorPalette = this.computeColors(s);

    for (const color of primaryColorPalette) {
      const key1 = `--theme-primary-${color.name}`;
      const value1 = color.hex;
      const key2 = `--theme-primary-contrast-${color.name}`;
      const value2 = color.darkContrast ? 'rgba(black, 0.87)' : 'white';
      document.documentElement.style.setProperty(key1, value1);
      document.documentElement.style.setProperty(key2, value2);
    }

    for (const color of secondaryColorPalette) {
      const key1 = `--theme-secondary-${color.name}`;
      const value1 = color.hex;
      const key2 = `--theme-secondary-contrast-${color.name}`;
      const value2 = color.darkContrast ? 'rgba(black, 0.87)' : 'white';
      document.documentElement.style.setProperty(key1, value1);
      document.documentElement.style.setProperty(key2, value2);
    }
  }

  computeColors(hex: string): Color[] {
    return [
      this.getColorObject(tinycolor(hex).lighten(52), '50'),
      this.getColorObject(tinycolor(hex).lighten(37), '100'),
      this.getColorObject(tinycolor(hex).lighten(26), '200'),
      this.getColorObject(tinycolor(hex).lighten(12), '300'),
      this.getColorObject(tinycolor(hex).lighten(6), '400'),
      this.getColorObject(tinycolor(hex), '500'),
      this.getColorObject(tinycolor(hex).darken(6), '600'),
      this.getColorObject(tinycolor(hex).darken(12), '700'),
      this.getColorObject(tinycolor(hex).darken(18), '800'),
      this.getColorObject(tinycolor(hex).darken(24), '900'),
      this.getColorObject(tinycolor(hex).lighten(50).saturate(30), 'A100'),
      this.getColorObject(tinycolor(hex).lighten(30).saturate(30), 'A200'),
      this.getColorObject(tinycolor(hex).lighten(10).saturate(15), 'A400'),
      this.getColorObject(tinycolor(hex).lighten(5).saturate(5), 'A700'),
    ];
  }

  getColorObject(value, name): Color {
    const c = tinycolor(value);
    return {
      name,
      hex: c.toHexString(),
      darkContrast: c.isLight(),
    };
  }

  loadPlaceConfig(place): void {
    this.placeBlockFree = false;
    this.place = place;
    console.log('place ::::', place);

    this.maxPeopleReservations = place?.maxPeopleReservations || 20;
    this.daysLock = [];
    for (const day of this.days) {
      let find = false;
      this.place.openingDays.forEach((elem) => {
        if (elem.weekday === day) {
          find = true;
        }
      });
      if (!find) {
        this.daysLock.push(this.traslate(day));
      }
    }

    this.coca = undefined;
    for (const item of this.place.sponsorsPlaces) {
      if (item.sponsorId === 1 && item.isActive) {
        this.coca = `${item.urlBase}/${this.place.id}.pdf`;
        break;
      }
    }

    if (!place.onlineSales) {
      this.step = 'onlineDisabled';
      return;
    }

    this.step = 'reservation';
    this.placeId = place.id;
    this.theme = place.theme;
    this.widgetBasic = place.typeWidget === 0; // 0 = Basico; 1 = Detallado;
    this.hasBooking = place.hasBooking; // reservar con horarios de servicios
    this.planFree = place.planId === 1 || (place.planId !== 1 && !this.hasBooking);

    if (this.place.advancePaymentPolicyEnable && (this.place.priorityPaymentWidget === 'advance' || !this.place.cancellationPolicyEnable)) {
      this.paymentConditional = 'advance';
    }

    if (this.place.cancellationPolicyEnable &&
      (this.place.priorityPaymentWidget === 'cancelation' || !this.place.advancePaymentPolicyEnable)) {
      this.paymentConditional = 'cancelation';
    }

    if (this.planFree || (!this.planFree && !this.hasBooking)) {
      // si el plan es free y reservar en horario de servicio esta deshabilitado.
      console.log('Load Times Horaries the work');
      this.loadTimes();
    } else {
      console.log('Load services Hotees');
      this.loadServices();
    }

    this.setValidatorsPriority();
    // this.onPeopleCountChange();
    if (this.verifyPlaceLock()) {
      this.placeBlockFree = true;
      this.getSuggestions();
    }
  }

  loadServices(): void {
    if (!this.planFree) {
      const r = this.form.value.reservation;
      this.widgetService
        .getServicesByDate(moment(r.date).format('YYYY-MM-DD'), this.placeId)
        .subscribe((services) => {
          this.services = services;
          if (
            this.form.get('reservation').get('date').value &&
            !this.daysLock.includes(
              this.form.get('reservation').get('date').value.format('dddd')
            )
          ) {
            this.loadTimes();
            this.onTimeChange();
          }
        });
    }
  }

  loadSmartTables(): void {
    const r = this.form.value.reservation;
    this.suggestion = [];
    const find = this.verifyPlaceLock();
    this.selectedTableGroup = undefined;
    if (
      this.selectedService &&
      r.peopleCount &&
      this.duration &&
      r.date &&
      r.time &&
      !find
    ) {
      const params = {
        peopleCount: r.peopleCount,
        duration: this.duration,
        date: moment(r.date).format('YYYY-MM-DD'),
        start: r.time,
        end: r.time + this.duration,
        smokeArea:
          r.area === 'SMOKE' ? true : r.area === 'NO_SMOKE' ? false : null,
        zoneId: r.zoneId || null,
        placeId: this.placeId,
        tableTypeId: r.tableTypeId,
        tableHeightId: r.tableHeightId,
        fromWidget: true,
      };
      this.widgetService
        .getSmartTables(this.selectedService.id, params)
        .subscribe((zonesTables) => {
          if (zonesTables.length) {
            const randomZone =
              zonesTables[Math.floor(Math.random() * zonesTables.length)];
            // console.log('zonesTables', zonesTables);
            // console.log('randomZone', randomZone);
            const tableGroups = randomZone.tableGroups;
            let tableGroupsFilter = tableGroups.filter((elem) => !elem.isGroup);
            // console.log('tableGroups', tableGroups);
            tableGroupsFilter =
              tableGroupsFilter.length > 0 ? tableGroupsFilter : tableGroups;
            // console.log('tableGroupsFilter', tableGroupsFilter);
            this.selectedTableGroup = tableGroups[Math.floor(Math.random() * tableGroupsFilter.length)]; // zonesTables[0].tableGroups[0];
            console.log('tableGroups', tableGroups);
            console.log('selected table', this.selectedTableGroup);
            if (!this.selectedTableGroup) {
              this.getSuggestions();
            }
          }
        });
    }

    if (find) {
      this.getSuggestions();
    }
  }

  getSuggestions(): void {
    const r = this.form.value.reservation;
    const params = {
      persons: r.peopleCount,
      duration: this.duration,
      date: moment(r.date).format('YYYY-MM-DD'),
      start: r.time,
      end: r.time + this.duration,
      minute:
        this.times.length > 0 ? this.times[this.times.length - 1].minute : 1400,
      firstMinute: this.times.length ? this.times[0].minute : 480,
      intervalWidget: this.place.intervalWidget ? this.place.intervalWidget : 1,
      isServices: !this.planFree,
    };
    if (this.times.length > 0) {
      console.log(this.times[this.times.length - 1]);
    }

    // this.widgetService.getSuggestions(this.place.id, params).subscribe(suggestions => {
    //   console.log(suggestions);
    //   let response = suggestions['suggestions'];
    //   let now;
    //   let lastday = [];
    //   for (let item of response) {
    //     // console.log(item);
    //     if (item.isToday) {
    //       for (let value of item.hours) {
    //         var m = moment().utcOffset(0);
    //         m.set({hour:0,minute:0,second:0,millisecond:0})
    //         value['displayHour'] = m.add(value['start'], 'minutes').format('H:mm');
    //       }
    //       now = item;
    //     } else {
    //       lastday.push(item)
    //     }
    //   }
    //   this.suggestion = [{ isToday: true, response: now}, {isToday: false, response: lastday}];
    //   // console.log(this.suggestion);
    // }, err => {
    //   console.log(err);
    // });
  }

  selectDate(item, isToday): void {
    this.placeBlockFree = false;
    if (isToday) {
      this.form.get('reservation').get('time').setValue(item.start);
      this.onTimeChange();
    } else {
      const day = moment(item.date);
      day.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
      day.add(item.start, 'minutes');
      // console.log(day.format());
      this.form.get('reservation').get('date').setValue(day);
      this.onDateChange();
    }
    this.suggestion = [];
  }

  getSmokeArea(area: string): boolean {
    switch (area) {
      case 'ANY':
        return null;
      case 'SMOKE':
        return true;
      case 'NO_SMOKE':
        return false;
    }
  }

  filterServiceZones(): void {
    switch (this.form.value.reservation.area) {
      case 'ANY':
        this.filteredServiceZones = this.serviceZones;
        break;
      case 'SMOKE':
        this.filteredServiceZones = this.serviceZones.filter(
          (sz) => sz.smokeArea
        );
        break;
      case 'NO_SMOKE':
        this.filteredServiceZones = this.serviceZones.filter(
          (sz) => !sz.smokeArea
        );
        break;
    }
  }
  onStep(value): void {
    this.step = value;
  }

  onReservationStep(waitList = false): void {
    if (this.form.get('reservation').valid) {
      const r = this.form.value.reservation;
      this.reservation.placeId = this.placeId;
      this.reservation.peopleCount = r.peopleCount;
      (this.reservation.datetime = moment(r.date)
        .startOf('day')
        .add(r.time, 'minutes')
        .format('YYYY-MM-DD[T]HH:mm:ss.000[Z]')),
        (this.reservation.date = moment(r.date).format('YYYY-MM-DD'));
      this.reservation.start = r.time;
      this.reservation.serviceId = this.planFree
        ? null
        : this.selectedService.id;
      this.reservation.end = this.planFree ? null : r.time + this.duration;
      this.reservation.duration = this.planFree ? null : this.duration;
      this.reservation.zoneId =
        this.planFree || waitList ? null : this.selectedTableGroup.zoneId;
      this.reservation.isGroup =
        this.planFree || waitList ? null : this.selectedTableGroup.isGroup;
      this.reservation.tableId =
        this.planFree || waitList
          ? null
          : this.selectedTableGroup.isGroup
          ? null
          : this.selectedTableGroup.id;
      this.reservation.groupId =
        this.planFree || waitList
          ? null
          : this.selectedTableGroup.isGroup
          ? this.selectedTableGroup.id
          : null;
      this.reservation.status = waitList
        ? 'WAIT_LIST'
        : this.planFree
        ? 'PENDING'
        : 'CONFIRMED';
      this.reservation.type = waitList ? 'FREE' : this.reservation.type;
      this.reservation.origin = this.origin;
      this.reservation.smokeArea = this.getSmokeArea(r.area);
      // this.reservation.notes = r.notes;

      this.step = 'customer';
    } else {
      Validations.validateForm(this.form.get('reservation') as FormGroup);
    }
  }

  onCustomerStep(): void {
    if (this.form.get('customer').valid) {
      const c = this.form.value.customer;
      this.reservation.customer = new Customer();
      if (this.sessionCustomer) {
        this.changeDisabled();
        this.form.controls.customer.get('email').enable();

        this.reservation.customer.fullname = this.form.value.customer.fullname;
        this.reservation.customer.countryCode =
          this.form.value.customer.countryCode;
        this.reservation.customer.phone = this.form.value.customer.phone;
        this.reservation.customer.email = this.form.value.customer.email;
        this.reservation.notes = this.form.value.customer.notes;
      } else {
        this.reservation.customer.fullname = c.fullname;
        this.reservation.customer.countryCode = c.countryCode;
        this.reservation.customer.phone = c.phone;
        this.reservation.customer.email = c.email;
        this.reservation.notes = c.notes;
      }

      if (!this.reservation.type) {
        this.reservation.type = 'FREE';
      }
      this.setValidators();
      console.log(this.reservation.type);
      if (this.reservation.type !== 'FREE' && this.reservation.type !== '') {
        this.step = 'payment';
      } else {
        this.onSubmit().then();
      }
    } else {
      Validations.validateForm(this.form.get('customer') as FormGroup);
    }
  }

  onStepStart(): void {
    if (this.sessionCustomer) {
      this.changeDisabledCustomer();
      this.form.get('customer').get('codeToRedeemW').setValue('');
      this.isActiveCode = 0;
      this.bonusCodePoints = 0;
    }

    this.reservation = new Reservation();
    this.reservation.partnerId = this.referralId;
    /* if (this.selectedSucursal) {
      this.form
        .get('reservation')
        .get('sucursal')
        .setValue(this.selectedSucursal.value);
      this.changeSucursal(this.selectedSucursal);
    } */
    if (!this.place) {
      this.loadConfig();
    } else {
      this.step = 'reservation';
    }
  }

  public async onPaymentStep(): Promise<void> {
    this.err = undefined;
    if (this.form.get('payment').valid) {
      const c = this.form.value.payment.card;
      // card.name = c.name;
      // card.number = c.number;
      // card.exp_year = c.exp_year;
      // card.exp_month = c.exp_month;
      // card.cvc = c.cvc;
      try {
        this.displayProgressSpinner = true;
        /* const token: ConektaToken = await this.widgetService.tokenize(
          card,
          this.placeId
        ); */
        // this.reservation.paymentTokenId = token.id;
        this.onSubmit().then();
      } catch (error) {
        console.log('error tarjeta', error);
        this.err = error?.message;
      } finally {
        this.displayProgressSpinner = false;
      }
    } else {
      Validations.validateForm(this.form.get('payment') as FormGroup);
    }
  }

  goToStep(step: string): void {
    this.step = step;
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      this.displayProgressSpinner = true;
      this.step = 'loading';
      this.setValidatorsPriority();
      // console.log(this.paymentConditional);
      if (this.viewPayment === 'advance') {
        this.reservation.advancePayment = true;
        this.reservation.adPayValue = this.charge;
      } else {
        this.reservation.advancePayment = false;
      }

      if (this.utms) {
        this.reservation.utmSource = this.utms.utmSource;
        this.reservation.utmMedium = this.utms.utmMedium;
        this.reservation.utmCampaign = this.utms.utmCampaign;
        this.reservation.utmTerm = this.utms.utmTerm;
      }

      console.log(this.reservation);
      this.reservation.partnerId = this.referralId ? this.referralId : 1;
      this.reservation.notificationEmail = true;
      this.widgetService.saveReservation(this.reservation).subscribe(
        (res: any) => {
          if (res.error) {
            this.step = 'err';
            this.msg = res.msg;
          } else {
            if (
              this.bonusCodePoints >= 500 &&
              this.form.value.customer.codeToRedeemW !== '' &&
              this.isActiveCode === 1
            ) {
              this.changeCode(res.id);
            }

            this.reservation.status = res?.status || this.reservation.status;
            const imageType =
              res.status === 'CONFIRMED' ? 'check' : 'reloj';
            this.imgUrl =
              'https://reservandonos-cdn.s3.us-west-2.amazonaws.com/icons/' +
              imageType +
              '.png';
            this.reservation.type = '';
            this.reservation.id = res.id;
            this.step = 'thanks';
          }
          if (
            this.reservation.partnerId === 1 &&
            this.reservation.origin === 'WEBSITE'
          ) {
            window.open(
              'https://reservandonos.com/gracias-por-tu-reserva/?' + res.id
            );
          }
        },
        (err: any) => {
          // console.log('esto es un error en save reservation');
          console.error(err.error);
          this.step = 'err';
          if (
            err?.error?.details &&
            err?.error?.details?.length > 0
          ) {
            if (
              err?.error?.details[0]?.code ===
              'conekta.errors.processing.bank.declined'
            ) {
              this.msg =
                'Las tarjetas han sido rechazadas. Inténtalo de nuevo.';
            } else if (
              err?.error?.details[0]?.code ===
              'conekta.errors.processing.bank.insufficient_funds'
            ) {
              this.msg =
                'No hay fondos suficientes en esta tarjeta. Inténtalo de nuevo';
            } else if (
              err?.error?.details[0]?.code ===
              'conekta.errors.processing.bank.msi_error'
            ) {
              this.msg =
                'No hay fondos suficientes en esta tarjeta. Inténtalo de nuevo';
            } else {
              this.msg = 'Error Inesperado';
            }
          } else {
            this.msg = err.error;
          }
        },
        () => {
          this.displayProgressSpinner = false;
        }
      );
    } else {
      Validations.validateForm(this.form);
    }
  }

  getAreaName(value: string): string {
    switch (value) {
      case 'SMOKE':
        return 'Fumar';
      case 'NO_SMOKE':
        return 'No fumar';
      default:
        return 'Cualquiera';
    }
  }

  getZoneName(id: number): string {
    if (id === 0) {
      return 'Cualquiera';
    }
    return this.filteredServiceZones.find((sz) => sz.zone.id === id).zone.name;
  }

  getTableTypeName(id: number): string {
    switch (id) {
      case 1:
        return 'Cuadrada';
      case 2:
        return 'Rectangular';
      case 3:
        return 'Redonda';
      default:
        return 'Cualquiera';
    }
  }

  getTableHeightName(id: number): string {
    switch (id) {
      case 1:
        return 'Alta';
      case 2:
        return 'Baja';
      default:
        return 'Cualquiera';
    }
  }

  getFormatedDate(date: Moment): string {
    return moment(date).format('dddd, D [de] MMMM [de] YYYY');
  }

  getFormatedTime(time: number): string {
    return moment().startOf('day').add(time, 'minutes').format('H:mm');
  }

  dateFilter = (date: Moment): boolean => {
    if (this.place) {
      const weekday = date.locale('en').format('dddd').toLowerCase();
      const od = this.place.openingDays.find((o) => o.weekday === weekday);
      return !!od;
    }
    return true;
  }

  loadTimes(): void {
    const selectedDate = moment(this.form.value.reservation.date);
    const midnight = moment().startOf('day');
    const weekday = selectedDate.locale('en').format('dddd').toLowerCase();
    const od: any = this.place.openingDays.find((o) => o.weekday === weekday);
    const isToday = moment().isSame(selectedDate, 'day');
    const currentMoment = moment().add(1, 'hour').startOf('hour');
    let currentMinute = currentMoment.diff(midnight, 'minutes');

    if (isToday && currentMoment.diff(moment(), 'minutes') < 30) {
      currentMinute =
        currentMinute +
        (currentMoment.diff(moment(), 'minutes') < 15 ? 30 : 15);
    }

    const interval = this.place.intervalWidget ? this.place.intervalWidget : 1;
    this.times = [];
    // if (this.planFree) {
      if (od) {
        let startMinute = od.start;
        let start2Minute = od.start2;
        const start = moment(midnight).add(startMinute, 'minutes');
        const start2 = moment(midnight).add(start2Minute, 'minutes');
        if (isToday) {
          if (currentMoment.isAfter(start)) {
            startMinute = currentMinute;
          }
          if (currentMoment.isAfter(start2)) {
            start2Minute = currentMinute;
          }
        }
        for (let i = startMinute; i < od.end; i += 15 * interval) {
          const time = {
            minute: i,
            format: moment(midnight).add(i, 'minutes').format('H:mm'),
          };
          if (!this.checkingLock(this.place, selectedDate.format('YYYY-MM-DD'), time.minute)) {
            if (!this.verifyPlaceLockTime(time.minute)) {
              this.times.push(time);
            }
          }
        }
        for (let i = start2Minute; i < od.end2; i += 15 * interval) {
          const time = {
            minute: i,
            format: moment(midnight).add(i, 'minutes').format('H:mm'),
          };

          if (!this.checkingLock(this.place, selectedDate.format('YYYY-MM-DD'), time.minute)) {
            if (!this.verifyPlaceLockTime(time.minute)) {
              this.times.push(time);
            }
          }
        }
        this.smokeArea = od.smokeArea;
      }
    if (this.times.length) {
      this.form.get('reservation').get('time').setValue(this.times[0].minute);
    }
    if (!this.smokeArea) {
      this.form.get('reservation').get('area').setValue('NO_SMOKE');
    }
  }

  checkingLock(place, date, time): boolean {
    if (place.isReservationsBlocked) {
      if (date >= place.blockedDateStart && date <= place.blockedDateEnd) {
        if (time >= place.blockedHourStart && time <= place.blockedHourEnd) {
          return true; // No inserta la hora
        } else {
          return false; // Inserta hora
        }
      } else {
        return false; // Inserta hora
      }
    }
    return false; // Inserta hora
  }

  enabledSuggestions(): void {
    if (this.planFree && this.verifyPlaceLock()) {
      this.placeBlockFree = true;
      this.getSuggestions();
    }
  }

  setValidators(): void {
    const f = this.form;
    const r = f.get('reservation');
    const c = f.get('customer');
    const p = f.get('payment');
    const d = p.get('card');

    // c.get('phone').setValidators([Validators.required]);
    // c.get('email').setValidators([Validators.required, Validators.email]);

    // if (c.value.email) {
    //   c.get('phone').setValidators(null);
    //   c.get('email').setValidators([Validators.required, Validators.email]);
    // }

    // if (c.value.phone) {
    //   c.get('phone').setValidators([Validators.required]);
    //   c.get('email').setValidators([Validators.email]);
    // }

    if (this.reservation.type === 'FREE') {
      d.get('number').setValidators(null);
      d.get('name').setValidators(null);
      d.get('exp_year').setValidators(null);
      d.get('exp_month').setValidators(null);
      d.get('cvc').setValidators(null);
      d.get('acceptCancellationPolicy').setValidators(null);
    } else {
      d.get('number').setValidators([
        Validators.required,
        // Validations.cardNumber,
      ]);
      d.get('name').setValidators([Validators.required]);
      d.get('exp_year').setValidators([
        Validators.required,
        // Validations.cardExpirationDate('exp_month', 'exp_year'),
      ]);
      d.get('exp_month').setValidators([
        Validators.required,
        // Validations.cardExpirationDate('exp_month', 'exp_year'),
      ]);
      d.get('cvc').setValidators([
        Validators.required,
        // Validations.cardCvc
      ]);
      d.get('acceptCancellationPolicy').setValidators([
        Validators.requiredTrue,
      ]);
    }

    d.get('number').updateValueAndValidity();
    d.get('name').updateValueAndValidity();
    d.get('exp_year').updateValueAndValidity();
    d.get('exp_month').updateValueAndValidity();
    d.get('cvc').updateValueAndValidity();
    d.get('acceptCancellationPolicy').updateValueAndValidity();

    c.get('phone').updateValueAndValidity();
    // c.get('email').updateValueAndValidity();
  }

  openHowToDialog(): void {
    const dialogRef = this.dialog.open(HowtoDialogComponent, {
      disableClose: false,
      data: {
        text: 'Conejo',
        colorTrue: 'warn',
        colorFalse: 'primary',
      },
      width: '440px',
    });
    dialogRef.afterClosed().subscribe(() => {});
  }

  openRedeemPointsDialog(): void {
    const dialogRef = this.dialog.open(RedeemPointsDialogComponent, {
      disableClose: false,
      data: {
        bonusCodePoints: this.bonusCodePoints,
      },
      width: '440px',
    });
    dialogRef.afterClosed().subscribe(() => {});
  }

  openErrorRedeemCodeDialog(message: any): void {
    const dialogRef = this.dialog.open(ErrorRedeemCodeDialogComponent, {
      disableClose: false,
      data: {
        message,
      },
      width: '440px',
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
      } else {
      }
    });
  }

  checkStatusCode(): void {
    const codeToRedeemW = this.form.value.customer.codeToRedeemW;
    this.widgetService
      .checkStatusCode(this.dataUser.id, codeToRedeemW)
      .subscribe(
        (response) => {
          if (response.success !== undefined) {
            this.openErrorRedeemCodeDialog(response.message);
          } else {
            this.bonusCodePoints = response.bonusCodePoints;
            this.isActiveCode = response.isActive;
            this.openRedeemPointsDialog();
          }
        },
        (err) => {
          this.openErrorRedeemCodeDialog('No se pudo canjear el codigo');
        }
      );
    // }
  }

  changeCode(reservationId): void {
    const requestBody = {
      id: this.dataUser.id, // idRegistercustomer
      code: this.form.value.customer.codeToRedeemW,
      reservationId
    };

    // Desactivar codigo
    this.widgetService.changeCode(requestBody).subscribe(
      (res) => {
        console.log(res);
      },
      (err) => {
        console.log(err);
      },
      () => {
        this.displayProgressSpinner = false;
      }
    );
  }

  changeDisabled(): void {
    this.form.controls.customer.get('fullname').enable();
    this.form.controls.customer.get('countryCode').enable();
    this.form.controls.customer.get('phone').enable();
  }

  changeDisabledCustomer(): void {
    this.form.controls.customer.get('fullname').disable();
    this.form.controls.customer.get('countryCode').disable();
    this.form.controls.customer.get('phone').disable();
    this.form.controls.customer.get('email').disable();
  }

  protected readonly environment = environment;
}
