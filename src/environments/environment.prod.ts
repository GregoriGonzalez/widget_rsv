export const environment = {
  production: true,
  apiUrl: 'https://api.reservandonos.com',
  urlImage : 'https://reservandonos-cdn.s3.us-west-2.amazonaws.com/',
  urlFlags: 'https://reservandonos-cdn.s3.us-west-2.amazonaws.com/flags'
};
