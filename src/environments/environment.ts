export const environment = {
  production: false,
  apiUrl: 'http://localhost:3000',
  urlImage: 'https://reservandonos-cdn.s3.us-west-2.amazonaws.com',
  urlFlags: 'https://reservandonos-cdn.s3.us-west-2.amazonaws.com/flags'
};
